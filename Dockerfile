FROM node:8.6.0-alpine

RUN apk update && apk add git

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
COPY package-lock.json /usr/src/app/
RUN NODE_ENV=development npm install -q

COPY . /usr/src/app

EXPOSE 5000
CMD [ "npm", "start" ]
